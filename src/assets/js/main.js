var circle,bets=100500,ms=1000;
var maxtime=5999999;
var timeleft=5999999;
var audioElement = document.createElement('audio');
audioElement.setAttribute('src', 'assets/sounds/audio.mp3');
var audioElement2 = document.createElement('audio');
audioElement2.setAttribute('src', 'assets/sounds/msg.mp3');
var audioElement3 = document.createElement('audio');
audioElement3.setAttribute('src', 'assets/sounds/open.wav');
var ls=0;
var roulet=0;
var timerinterval;
window.onload = function onLoad() {
	circle = new ProgressBar.Circle('#prograsd', {
		color: '#3a3f42',
		strokeWidth: 12,
		easing: 'easeInOut',
		trailColor: "#76cfe2"
	});
	circle.animate(1);

	reloadinfo();

	timerinterval = setInterval("reloadinfo()",1000);
	setInterval("updatetimer()",1000);
	setInterval("importantvalue()",1000);
};

function alert2(txt,typet) {
	var n = noty({
		layout: 'bottomRight',
		text: txt,
		type: typet,
		timeout: 10000
	});
	audioElement.play();
}

function updatetimer() {
	var d = new Date();
    var n = 99-Math.round(d.getMilliseconds()/10);
	if(timeleft == maxtime) n = 0;
	if(n < 0) n = 0;
	if(timeleft <= 0) n = 0;
	if(timeleft < 0) timeleft = 0;
	if(n < 10) $('#timeleft').text(timeleft+"s");
	else $('#timeleft').text(timeleft+"s");
}

function reloadinfo() {
	$.ajax({
		type: "GET",
		url: "ajax/currentinfo.php",
		success: function(data){
			var info = JSON.parse(data);
			
			if(info.items.value>info.maxritems.value) {info.items=info.maxritems};

			circle.animate(info.items/info.maxritems);
			$('.progressbar__label').text(info.items+'/'+info.maxritems);
			
			$('#bank').text(info.bank+'');

			$("#mychance").text(info.chance);		

			$("#gameid").text("#"+info.game);		

			timeleft = info.time;
			maxtime = info.maxtime;
		}
	});
	
}

function importantvalue() {
	$.ajax({
		type: "GET",
		url: "ajax/items.php",
		success: function(msg){
			$('.rounditems').html(msg);
		}
	});
}