<?php
function isbanned($steamid){
	$bannedlist=file_get_contents($_SERVER['DOCUMENT_ROOT'].'/chat/chatbanned.txt');

	if(preg_match('#'.$steamid.'#', $bannedlist) && !isadmin($steamid))
		return true;
	else
		return false;
}

function ismuted($steamid){
	$mutedlist=file_get_contents($_SERVER['DOCUMENT_ROOT'].'/chat/chatmuted.txt');
	if(preg_match('#'.$steamid.'#', $mutedlist) && !isadmin($steamid))
		return true;
	else
		return false;
}

function chaton(){
	if(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/chat/chattoggle.txt')=='OFF')
		return false;
	else
		return true;
}