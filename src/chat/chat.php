<?php 
ini_set('display_errors', 1);
session_start();
include $_SERVER['DOCUMENT_ROOT'].'/set.php';
include $_SERVER['DOCUMENT_ROOT'].'/chat/adminlist.php'; //include AFTER session is started (session_start();)
include $_SERVER['DOCUMENT_ROOT'].'/chat/chatfunctions.php'; //include AFTER including adminlist.php
require_once $_SERVER['DOCUMENT_ROOT'].'/steamauth/userInfo.php';

$file = $_SERVER['DOCUMENT_ROOT'].'/chat/chat.txt';
if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    $today = date("H:i:s | d.m.Y");
    if ($_POST['massage'] == '') {
        die('<script>alert("Error! The message can\'t be empty!");</script>');
    }
    if (strlen($_POST['massage']) < 3) {
        die('<script>alert("Error! Very short message min. 3 characters")</script>');
    }
    if (strlen($_POST['massage']) > 300) {
        die('<script>alert("Error! Very long message, max. 300 characters!")</script>');
    }
    //die('<script>alert("'.isbanned($steamprofile['steamid']).'")</script>');
    if (isbanned($steamprofile['steamid'])){
        die('<script>alert("Error! You are banned from using the chat.")</script>');
    }
    if (ismuted($steamprofile['steamid'])){
        die('<script>alert("Error! You are muted from chat by an admin. The punishment lasts 24hrs.")</script>');
    }
    if(!isset($_SESSION['steamid'])) {
        echo '<script>alert("Error! Only logged in users can write in chat!")</script>';
    }else {
        $massage=htmlspecialchars($_POST['massage']); 
        if(!isadmin($steamprofile['steamid'])) { 
            
            $rs3 = mysql_query("SELECT word FROM `chat_badwords`");
            $badwords = Array();
            while($row = mysql_fetch_assoc($rs3)){
                array_push($badwords, $row['word']);
            }

            foreach($badwords as $word){
                if(preg_match('#'.$word.'#i', $massage)) {
                    echo '<script>alert("Our system thinks that you may have used an innapropriate word or phrase in the chat and it was censored. If you are using innapropriate language or you are spamming you will get banned.")</script>';
                    mysql_query("UPDATE chat_badwords SET `used`=`used`+1 WHERE `word`='$word'");
                }

            }

            $massage=str_ireplace($badwords, '****', $massage); 
        }


    if(isadmin($steamprofile['steamid'])) { 
        $steamprofile['personaname']= '<font color=#880000><b>'.$steamprofile['personaname'].'</b></font>';
        $massage = '<font color="#EA7526"> <i>'.$massage.'</i></font>';
    }

    $personoriginal = '<div class="chat-msg">
                <div class="caht-ava"><img src="'.$steamprofile['avatarmedium'].'" width="30px"></div>
                <div class="caht-name"><a href="'.$steamprofile['profileurl'].'" target="_blank">'.$steamprofile['personaname'].'</a></div>
                <div class="caht-dateid">'.$today.'</div>
                <div class="msg-text">'.$massage.'</div>
            </div>';

    $person = '
                </div>
               '.$massage.'
                <div class="caht-name"><img class="caht-ava" src="'.$steamprofile['avatarmedium'].'" width="20px"><a href="/chat/prredirect.php?id='.$steamprofile['steamid'].'&amp;name='.base64_encode($steamprofile['personaname']).'" target="_blank" title="'.$today.'">'.$steamprofile['personaname'].'</a>:</div>
                <div class="chat-msg">

            ';
    file_put_contents($file, $person, FILE_APPEND | LOCK_EX);
    }
    exit;
}