<?php
ini_set('display_errors', 1);
require_once "langdoc.php";

require_once('set.php');

require_once('steamauth/steamauth.php');
require_once('steamauth/userInfo.php');

?>

<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="steam jackpot system">
  <meta name="author" content="ufogufo13@gmail.com">

  <title>jackpot </title>

  <link rel="stylesheet" href="./assets/normalize.css">
  <link rel="stylesheet" href="./assets/css.css">
  <link rel="stylesheet" href="./assets/jackpot.css">
  <link rel="stylesheet" href="/assets/css/navbar-static-top.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/noty/packaged/jquery.noty.packaged.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="/assets/js/ie10-viewport-bug-workaround.js"></script>

  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="cover" style="overflow: visible; margin-right: 0px;">
  <div class="result success" id="success"></div>
  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/index.php">Jackpot system</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a href="/index.php"><?php echo $msg[$lang]["play"] ;?></a></li>
          <li class="active"><a href="/history.php"><?php echo $msg[$lang]["history"] ;?></a></li>
          <li><a href="/top.php"><?php echo $msg[$lang]["top"] ;?></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
        <?php if(!isset($_SESSION["steamid"])) { ?>
          <li><a href="?login"><?php echo $msg[$lang]['authwst']; ?></a></li>
        <?php } else{ ?>
          <li><img class="avatarimage" src="<?php echo $steamprofile['avatar']; ?>"></li>
          <li><a href="steamauth/logout.php"><?php echo $msg[$lang]["logout"]; ?></a></li>
        <?php } ?>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>
  <div class="container">
    <div class="jumbotron">

      <?php 
        if(!isset($_SESSION["steamid"])) {
            steamlogin();
        }else { 
            mysql_query("UPDATE users SET name='".$steamprofile['personaname']."', avatar='".$steamprofile['avatarfull']."' WHERE steamid='".$_SESSION["steamid"]."'");
        } 
        $gamenum = fetchinfo("value","info","name","current_game");
        $rs = mysql_query("SELECT * FROM `games` WHERE `id` < $gamenum ORDER BY `id` DESC LIMIT 100");
        while($row = mysql_fetch_array($rs)) {
            $lastwinner = $row["userid"];
            $winnercost = $row["cost"];
            $winnerpercent = $row["percent"];
            $winneravatar = fetchinfo("avatar","users","steamid",$lastwinner);
            $winnername = fetchinfo("name","users","steamid",$lastwinner);            
            $idsteam = fetchinfo("steamid","users","steamid",$lastwinner);
            ?>
            <div class="row">
                <ul class="history">
                    <h3>Game id: <?php echo $row["id"]; ?></h3>
                    <div class="col-sm-4">
                        <li>
                            <div>
                                <p><?php echo $winnername; ?></p>
                                <a class="history_user_steamavatar" href="http://steamcommunity.com/profiles/<?php echo $idsteam; ?>" target="_blank"><img src="<?php echo $winneravatar; ?>" /></a>
                            </div> 
                        </li>
                        <li><?php echo $msg[$lang]["wcha"].': '.round($winnerpercent,1); ?>%</li>
                        <li><?php echo $msg[$lang]["wwin"].': '.round($winnercost,2); ?>$</li>
                    </div>
                    <div class="col-sm-8">
                        <div class="won_items">
                        <?php 
                        $rs2 = mysql_query("SELECT * FROM `games_info` WHERE `game_id`='{$row['id']}'");
                        while($row2 = mysql_fetch_array($rs2)) { ?>
                            <a target="_blank" href="https://steamcommunity.com/market/listings/730/<?php echo $row2["item"]; ?>">
                                <img src="http://steamcommunity-a.akamaihd.net/economy/image/<?php echo $row2["image"]; ?>/60fx60f" width="60" height="60">
                            </a>
                        <?php } ?>
                        </div>
                    </div>
                </ul>
            </div>
        <?php } ?>
      </div>
  </div>
</body>

</html>
