 <?php
 	

include $_SERVER['DOCUMENT_ROOT'].'/admin/components/adm_head.php' ;
?>
<body class="wysihtml5-supported">
    <!-- Navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <div class="hidden-lg pull-right">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-right">
                        <span class="sr-only"></span>
                        <i class="fa fa-chevron-down"></i>
                    </button>

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar">
                        <span class="sr-only">Toggle sidebar</span>
                        <i class="fa fa-bars"></i>
                    </button>
                </div>

                <ul class="nav navbar-nav navbar-left-custom">
                    <li class="user dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo $steamprofile['avatarfull']; ?>" alt="">
                            <span><?php echo $steamprofile['personaname'] ;?></span>
                        </a>
                    </li>
                    <li><a class="nav-icon sidebar-toggle"><i class="fa fa-bars"></i></a></li>
                </ul>
			</div>
			<ul class="nav navbar-nav navbar-right collapse" id="navbar-right">
                <li>
                    <a href="/">
                        <i class="fa fa-sign-out"></i>
                        <span>Home</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
        <div class="page-header">
			<div class="panel-body" style=" width: 400px; left: 285px; top: 36px; position: absolute; "></div>
            <ul class="middle-nav">
            </ul>
        </div>
    </div>


    <div class="page-container container-fluid">
        <div class="sidebar collapse">
        	<ul class="navigation">
            	<li><a href="/admin/"><i class="fa fa-home "></i>Dashboard</a></li>
                <li><a href="/admin/games"><i class="fa fa-gamepad"></i>Games</a></li>
                <li><a href="/admin/users"><i class="fa fa-user"></i>Users</a></li>
                <li><a href="/admin/items"><i class="fa fa-user"></i>Items</a></li>
                <li><a href="/admin/bot"><i class="fa fa-user"></i>Bot</a></li>
            </ul>
			<div class="col-md-6" style=" width: 260px; right: 15px; top: 10px; ">
            </div>
        </div>

        <div class="page-content">
        	<div class="page-title">
                <h5><i class="fa fa-bars"></i>  <?php echo$steamprofile['personaname']; ?> <small>, Welcome! </small></h5>
            </div>